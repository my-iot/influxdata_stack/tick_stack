# TICK_Stack

TICK stack implementation based on Docker-Compose.

# What is TICK Stack?

The **TICK stack** is a collection of open source tools from **InfluxData** that offer collection, storage, graphing, and alerting for time series data. This is especially useful for **IoT** where real-time monitoring of sensors and devices is critical.

## Components of TICK Stack

First, let’s take a quick look at the open source tools that make up the TICK stack

* **Telegraf:** Telegraf is the server **agent for collecting and sending data** to be stored. It integrates with APIs, containers, Kafka, and many other systems for data collection. It can send data to other databases and systems such as InfluxDB, CloudWatch, Cassandra, and over 200 other integrations.
* **InfluxDB:** InfluxDB is an open source **time series database** that uses an SQL-like syntax called InfluxQL to write queries. It can handle millions of data points per second and compacts data to save space, which is great for something like IoT devices that continuously send data.
* **Chronograf:** Chronograf is the real-time **visualization dashboard** for the stack. It comes with templates to get you started visualizing your InfluxDB data. It also has monitoring and alerting features that can be set so you’re notified of any deviations that should be investigated.
* **Kapacitor:** Kapacitor **processes streaming data** in real-time and can **perform advanced analytics** before sending data to InfluxDB to be stored. It also has a publish/subscribe model for alerting. This can be used to invoke user-defined functions based on thresholds or send alerts based on data received from IoT devices. These alerts can be published to tools like PagerDuty and Slack.

    ![TICK STACK](images/tick_stack.png)

# Installation/Deployment

## Pre-Requisites
In order to deploy the TICK stack we will need the following:
* Docker engine
* Docker-compose

## Install the TICK Stack

we will us the following "docker-compose.yml" configuration:

```zsh
 version: '3'
 
 services:
   # Define a Mosquitto (MQTT) service
   mosquitto:
     image: eclipse-mosquitto:latest
     volumes:
       - ./mosquitto/data:/mosquitto/data
       - ./mosquitto/log:/mosquitto/log
       - ./mosquitto/mosquitto.conf:/mosquitto/config/mosquitto.conf
     restart: always
     ports:
       - "1883:1883"
       - "9001:9001"
 
   # Define a Telegraf service
   telegraf:
     image: telegraf:latest
     volumes:
       - ./telegraf/telegraf.conf:/etc/telegraf/telegraf.conf
     links:
       - influxdb
     restart: always
     ports:
       - "8092:8092/udp"
       - "8094:8094"
       - "8125:8125/udp"
 
   # Define an InfluxDB service
   influxdb:
     image: influxdb:latest
     volumes:
       - ./influxdb/data/influxdb:/var/lib/influxdb
     restart: always
     ports:
       - "8086:8086"
 
   # Define a Chronograf service
   chronograf:
     image: chronograf:latest
     volumes:
       - ./chronograf:/var/lib/chronograf
     environment:
       INFLUXDB_URL: http://influxdb:8086
       KAPACITOR_URL: http://kapacitor:9092
     restart: always
     ports:
       - "8888:8888"
     links:
       - influxdb
       - kapacitor
 
   # Define a Kapacitor service
   kapacitor:
     image: kapacitor:latest
     volumes:
       - ./kapacitor/kapacitor.conf:/etc/kapacitor/kapacitor.conf
     environment:
       KAPACITOR_HOSTNAME: kapacitor
       KAPACITOR_INFLUXDB_0_URLS_0: http://influxdb:8086
     links:
       - influxdb
     restart: always
     ports:
       - "9092:9092"

```

## Launch the Stack

First we will enter the directory where the docker-compose file is stored and make a "docker-compose up":

```zsh
$docker-compose up
```

Then, we can **check the current status** of the docker-compose with the following command:

```zsh
$docker-compose ps
```

In order to **start/stop** the docker-compose we will proceed as follows:

```zsh
$docker-compose stop
$docker-compose start
```

To **stop and remove** the docker-compose:

```zsh
$docker-compose down
 ```
